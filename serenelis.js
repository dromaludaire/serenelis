function setQuery() {
    wq = $('#currentQuery').text();
    window.location = 'index.php/setQuery/' + encodeURIComponent(wq);
}

function resetQuery() {
    window.location = 'index.php/setQuery/all';
}

function historyBack() {
    window.location = 'index.php/back/';
}

function historyForward() {
    window.location = 'index.php/forward/';
}

function copyPasteTo(feat, oid) {
    window.location = 'index.php/copyPasteObjectTo/' + encodeURIComponent(feat) + '/' + oid;
}

function addAxiom(prem, concl) {
    window.location = 'index.php/addAxiom/' + encodeURIComponent(prem) + '/' + encodeURIComponent(concl);
}



$(document).ready(function(){
    $('#currentQuery').keypress(function(event){
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
		    setQuery();
	    }
    });

    $('.button').button();
    $('#logicalConnectors').buttonset();

    $('#features').on('click', 'a', function() {
        featname = $(this).find('.featname').first().text();

        connector = $('#logicalConnectors input:checked').val();
        window.location = 'index.php/updateQuery/' + connector + '/' + featname;
    });

    // expand/retract features
    $('#features').on('click', '.expandButton', function() {
        feat_li = $(this).parent();
        featname = feat_li.find('.featname').first().text();
        $.ajax({ url: "index.php/expandFeature/" + featname,
                 async: false,  // needed for expandAll to work properly
               })
            .done(function(msg) {
                feat_li.append(msg);
            });
        $(this).toggleClass('expandButton retractButton');
    });
    $('#features').on('click', '.retractButton', function() {
        $(this).siblings('ul').remove();
        $(this).toggleClass('expandButton retractButton');
    });

    // expand/collapse all features
    $('#expand_all').click(function() {
        $('.expandButton').click();
    });

    $('#collapse_all').click(function() {
        $('.retractButton').click();
    });

    // drag'n'drop for modifying object description
    $('#extension li').draggable({
        revert: true,
        helper: 'clone',
        cursor: 'move'
    });

    // This is a small hack to attach the draggable property to features
    // that appeared after page creation (e.g. using the expand button)
    $('#features').on('mouseenter', 'li', function() {
        if (!$(this).is(':data(draggable)')) {
            $(this).draggable({
                revert: true,
                helper: 'clone',
                cursor: 'move'
            });
        }
    });

    // Same hack for droppable expanded features
    $('#features').on('mouseenter', 'li', function() {
        if (!$(this).is(':data(droppable)')) {
            $('#features li').droppable({
                accept: '#extension li, #features li',
                hoverClass: 'ui-state-highlight',
                greedy: true,
                drop: function(event, ui) {
                    var msg;
                    var okFunction;
                    var validDrop = true;
                    var connector = $('#logicalConnectors input:checked').val();
                    // are we dealing with an object?
                    if (ui.draggable.parents('#extension').length == 1) {
                        var oid = ui.draggable.data('oid');
                        var feat = $(this).find('.featname').first().text();
                        msg = 'Add property ' + feat + ' to object #' + oid + '?';
                        if (connector === 'not') {
                            msg = 'Remove property ' + feat + ' from object #' + oid + '?';
                            feat = 'not ' + feat;
                        }
                        okFunction = function() {
                            copyPasteTo(feat, oid);
                            $(this).dialog('close');
                        };
                    } else {     // we're dealing with a feature, then
                        var premisse = ui.draggable.find('.featname').first().text();
                        var concl = $(this).find('.featname').first().text();
                        // check if we're dropping on ourself
                        if (premisse == concl) {
                            validDrop = false;
                        }
                        msg = 'Add axiom ' + premisse + ' => ' + concl + '?';
                        if (connector === 'not') {
                            msg = 'Remove axiom ' + premisse + ' => ' + concl + '?';
                            concl = 'not ' + concl;
                        }
                        okFunction = function() {
                            addAxiom(premisse, concl);
                            $(this).dialog('close');
                        };
                    }
                    if (validDrop) {
                        $('<div>' + msg + '</div>').dialog({
                            modal: true,
                            buttons: {
                                Ok: okFunction,
                                Cancel: function() {
                                    $(this).dialog('close');
                                }
                            }
                        });
                    }
                }
            });
        }
    });

});
