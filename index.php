<?php
require 'vendor/autoload.php';

/* Use PHP session store */
session_cache_limiter(false);
session_start();


$app = new \Slim\Slim();


/* We define where the templates are stored */
$app->config(array(
   'templates.path' => './templates'
));


function flog($msg) {
    $fd = fopen('/tmp/serenelis.log', 'a');
    $time = @date('[d/M/Y:H:i:s]');
    fwrite($fd, "{$time} {$msg}" . PHP_EOL);
    fclose($fd);
}

/* lis server address */
/* we assume xml-lis-server was launched this way: */
/* ./xml-lis-server.exe -port 9999 -creator creator@example.com -key abcd -serviceId foo:bar::serenelis */
/* ctx file should therefore be present in /tmp/serenelis */

$lis_server = 'http://localhost:9999/';
$lis_admin_key = 'a243F';

if (!isset($_SESSION['wq'])) {
    $_SESSION['wq'] = 'all';
}

/* History */
if (!isset($_SESSION['history_back'])) {
    $_SESSION['history_back'] = array();
}
if (!isset($_SESSION['history_fwd'])) {
    $_SESSION['history_fwd'] = array();
}

$app->get('/', function () use ($app, $lis_server, $lis_admin_key) {
        $lis_wq = $_SESSION['wq'];

        //TODO: try/catch around simplexml_load_from_file, render some error page if necessary
        //TODO: + do it everywhere
        $extent_url = $lis_server . 'extent?userKey=' . $lis_admin_key . '&query=' . $lis_wq;
        $extent = simplexml_load_file($extent_url);

        $features_url = $lis_server . '/zoom?feature=all&wq=' . $lis_wq . '&userKey=' . $lis_admin_key;
        $features = simplexml_load_file($features_url);

        // the zoom command returns a normalized working query, we use it here
        $_SESSION['wq'] = $features['newWq'];

        $data = array(
             /* TODO: should get rid of baseUrl with some clever url-rewriting */
            'baseUrl' => $app->request->getRootUri(),
            'histBack' => $_SESSION['history_back'],
            'histFwd' => $_SESSION['history_fwd'],
            'wq' => $lis_wq,
            'extent' => $extent->extent,
            'features' => $features->increments,
            );
        $app->render('main.html', $data);
    });



$app->get('/updateQuery/:connector/:incr', function ($connector, $incr) use ($app) {
        $old_wq = $_SESSION['wq'];
        array_push($_SESSION['history_back'], $old_wq);
        $_SESSION['history_fwd'] = array();
        if ($connector === 'not') {
            $connector = 'and';
            $incr = "not {$incr}";
        }
        $_SESSION['wq'] = $old_wq === 'all' ? $incr : "{$old_wq} ${connector} {$incr}";
        flog("action:updateQuery connector:{$connector} incr:{$incr}");
        $app->response->redirect($app->request->getRootUri());
    });


$app->get('/setQuery/:wq', function ($wq) use ($app) {
        $old_wq = $_SESSION['wq'];
        array_push($_SESSION['history_back'], $old_wq);
        $_SESSION['history_fwd'] = array();
        $_SESSION['wq'] = $wq;
        flog("action:setQuery new_wq:{$wq}");
        $app->response->redirect($app->request->getRootUri());
    });

$app->get('/back/', function () use ($app) {
        if (!empty($_SESSION['history_back'])) {
            array_push($_SESSION['history_fwd'], $_SESSION['wq']);
            $wq = array_pop($_SESSION['history_back']);
            $_SESSION['wq'] = $wq;
            flog("action:history_back new_wq:{$wq}");
        }
        $app->response->redirect($app->request->getRootUri());
    });

$app->get('/forward/', function () use ($app) {
        if (!empty($_SESSION['history_fwd'])) {
            array_push($_SESSION['history_back'], $_SESSION['wq']);
            $wq = array_pop($_SESSION['history_fwd']);
            $_SESSION['wq'] = $wq;
            flog("action:history_forward new_wq:{$wq}");
        }
        $app->response->redirect($app->request->getRootUri());
    });


$app->get('/expandFeature/:featname', function ($featname) use ($app, $lis_server, $lis_admin_key) {
        $lis_wq = $_SESSION['wq'];
        $url = "{$lis_server}/zoom?feature={$featname}&wq={$lis_wq}&userKey={$lis_admin_key}";
        $expanded_feat = simplexml_load_file($url);

        $res = '<ul>';
        foreach ($expanded_feat->increments->increment as $incr) {
            $name = $incr['name'];
            $card = $incr['card'];
            $res = $res . "<li><div class='expandButton'></div><a href='#'><span class='featname'>{$name}</span> (${card})</a></li>";
        }
        $res = $res . '</ul>';

        flog("action:expandFeature feature_name:{$featname}");
        $app->response->write($res);
    });

$app->get('/copyPasteObjectTo/:featname/:oids+', function ($featname, $oids) use ($app, $lis_server, $lis_admin_key) {
        $lis_wq = $_SESSION['wq'];
        $url = "${lis_server}/setFeaturesFromOids?userKey=${lis_admin_key}&query=${lis_wq}&feature={$featname}";
        foreach ($oids as $oid) {
            $url = $url . "&oid=" . $oid;
        }

        $res = simplexml_load_file($url);
        if ($res->setFeaturesFromOidsResponse['status'] !== 'ok') {
            // TODO: handle error
        }
        foreach ($oids as $oid) {
            flog("action:copyPasteObjectTo oid:{$oid} feature:{$featname}");
        }
        $app->response->redirect($app->request->getRootUri());
    });


$app->get('/addAxiom/:prem/:concl', function ($prem, $concl) use ($app, $lis_server, $lis_admin_key) {
        $url = "${lis_server}/addAxiom?userKey=${lis_admin_key}&premise=${prem}&conclusion={$concl}";
        $res = simplexml_load_file($url);
        if ($res->addAxiomResponse['status'] !== 'ok') {
            // TODO: handle error
        }
        flog("action:addAxiom premisse:{$prem} conclusion:{$concl}");
        $app->response->redirect($app->request->getRootUri());
    });


$app->run();

?>