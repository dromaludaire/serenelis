<?php
session_start();

if(isset($_GET["log_out"])){
  unset($_SESSION["logged_in"]);
  echo "You're logged out, and will be redirected in about 3 seconds";
  header('refresh: 3; url=login.php');
  exit;
}

$login = true;
require "protect.php";

$logins[0]["user"] = "admin";
$logins[0]["pass"] = "serenelis";
$logins[0]["redirect"] = "admin.php";

$logins[1]["user"] = "guest";
$logins[1]["pass"] = "serenelis";
$logins[1]["redirect"] = "index.php";

// No need to edit below, except the errors

if(isset($_POST['submit'])){ //is the form submitted?
  if(empty($_POST['user']) || empty($_POST['pass'])){
    echo "You have to fill out the user name and password!";
    exit;
  } //check for empty user name or password
  $is_logged = false; //this is part of the process to see if they have a correct password or not, set to false right here to say no right pass... (will change later)
  foreach($logins as $login){
    $user = $_POST;
    if(($user["user"] == $login["user"]) && ($user["pass"] == $login["pass"])) {
      $is_logged = true;
      $_SESSION["logged_in"] = array($login["redirect"], true); //now, if they do have a correct password, set the session to true, and the variable.
      header("Location: ".$login["redirect"]);
    }
  }
  if(!$is_logged){ echo "Username/password did not match, try again!"; } //if none of the $logins arrays matched the input, give an error
}
?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
Username:<br />
<input type="text" name="user" /><br />
Password:<br />
<input type="password" name="pass" /><br />
<input type="submit" name="submit" value="Log in!" />
</form>
