<?php
//Сheck that we have a context file
if((!empty($_FILES["uploaded_file"])) && ($_FILES['uploaded_file']['error'] == 0)) {
  //Check if the file is CTX (context) and it's size is less than 3500Kb
  $filename = basename($_FILES['uploaded_file']['name']);
  $ext = substr($filename, strrpos($filename, '.') + 1);
  if (($ext == "ctx")  &&   ($_FILES["uploaded_file"]["size"] < 35000000)) {
    //Determine the path to which we want to save this file
      $newname = dirname(__FILE__).'/context/'.$filename;
      //Check if the file with the same name is already exists on the server
      if (!file_exists($newname)) {
        //Attempt to move the uploaded context file to it's new place
        if ((move_uploaded_file($_FILES['uploaded_file']['tmp_name'],$newname))) {
           echo "It's done! The context file has been saved as: ".$newname;
        } else {
           echo "Error: A problem occurred during context file upload!";
        }
      } else {
         echo "Error: Context file ".$_FILES["uploaded_file"]["name"]." already exists";
      }
  } else {
     echo "Error: Only .ctx contexts under 3500Kb are accepted for upload";
  }
} else {
 echo "Error: No file context uploaded";
}
?>
